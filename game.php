<!DOCTYPE html>
<html>
<head>
		<meta charset="UTF-8" />
		<title>MovIIE Quizz</title>
		<link rel="stylesheet" href="main.css" type="text/css">
</head>
<body class="BG">
  <header class="Game-header">
    <div class="Logo"><img class="Logo" src="resources/MovIIEQuizz.png" alt="Logo"></div>
  </header>
  <div class="MainBody">
     <!--
     <div id="bip" class="timer" onload="start()">Time : 90</div>
     <div id="score" class="score">Score : 0</div>
      -->

      <div id="game">
        
        <div class="GameInfo">
          <div class="Indicator">
            <img src="resources/time.png" alt="icon" class="Icon">
            <span class="Text timer" id="bip" onload="start()">00:59</span>
          </div>

          <div class="Indicator">
            <img src="resources/trophy.png" alt="icon" class="Icon">
            <span class="Text score" id="score" >0</span> <br/>
            <img src="resources/star.png" alt="icon" class="Icon" style="height: 20px">
            <span class="Text score" id="best_score" style="font-size: 18px">0</span> <!-- meilleur score a afficher en haut à droite -->
          </div>


        </div>

          <!--<p><img src="rambo.jpg" class="affiche"></p>-->

          <div class="MovieCard affiche">
            <img src="resources/films/Robocop.jpg" alt="poster" id = "poster" class="Poster">
            <h3 class="Title" id = "title">Rambo</h3>
          </div>
          

        
      <div class="ButtonGroup">
        
        <div class="actor-choice ButtonDuo">
          <button class="ActorButton" id="button-1" onclick="wrong_choice(event)" >Christian Clavier</button>
          <button class=" ActorButton" id="button-2" onclick="right_choice(event)"> Sylverster Stalone</button>
        </div>
        <div class="actor-choice ButtonDuo">
          <button class=" ActorButton" id="button-3" onclick="wrong_choice(event)" >Jean Dujardin</button>
          <button class=" ActorButton" id="button-4" onclick="wrong_choice(event)"> Keanu Reeves</button>
        </div>
      </div>
        
      </div>
    </div>


    <!-- The Modal -->
    <div id="myModal" class="modal" style="text-align: center">

      <!-- Modal content -->
      <div class="modal-content">
        <p class="Title" style="font-size: 48px">Game Over!</span></p>
        <div>
          <p class="Text" >Score:<b><span class="BestScore" id="finalBestScore" >0</span></b></p>
        </div><!--
        <div>
          <p class="Text">Player Name : <input id="playerName" type="text" /></p>
        </div>-->
        <div class="actor-choice ButtonDuo">
          <button class="ActorButton" onclick="saveScore()" > Play Again</button>
        </div>
      </div>

    </div>


</body>
<script src="require.js" type="text/javascript"></script>

<script>
var counter = 60; // Counter 
var filmList = [["A Nightmare on Elm Street 3_ Dream Warriors","Robert Englund"],
["A Nightmare on Elm Street","John Saxon"],
["A View to a Kill","Roger Moore"],
["Alien","Tom Skerritt"],
["Amadeus","F. Murray Abraham"],
["Animal House","John Belushi"],
["Apocalypse Now","Marlon Brando"],
["Apocalypto","Rudy Youngblood"],
["Back to the Future Part II","Christopher Lloyd"],
["Back to the Future Part III","Michael J. Fox"],
["Back to the Future","Christopher Lloyd"],
["Beetlejuice","Michael Keaton"],
["Big Trouble in Little China","Kurt Russell"],
["Blade Runner","Harrison Ford"],
["Brazil","Jonathan Pryce"],
["Candyman","Virginia Madsen"],
["Dark City","Rufus Sewell"],
["Diamonds Are Forever","Sean Connery"],
["Dirty Harry","Clint Eastwood"],
["Dirty Rotten Scoundrels","Steve Martin"],
["Dogma","Ben Affleck"],
["Drag Me to Hell","Alison Lohman"],
["Enemy","Jake Gyllenhaal"],
["Enter the Dragon","Bruce Lee"],
["Escape from New York","Kurt Russell"],
["European Vacation","Chevy Chase"],
["Fear and Loathing in Las Vegas","Johnny Depp"],
["Freddy's Dead: The Final Nightmare","Robert Englund"],
["Gremlins","Zach Galligan"],
["Halloween II","Scout Taylor-Compton"],
["Hook","Robin Williams"],
["Indiana Jones and the Last Crusade","Harrison Ford"],
["Indiana Jones and the Temple of Doom","Harrison Ford"],
["Inglourious Basterds","Brad Pitt"],
["Inherent Vice","Joaquin Phoenix"],
["Interstellar","Matthew McConaughey"],
["It Follows","Maika Monroe"],
["Jaws","Roy Scheider"],
["Killer Klowns from Outer Space","Grant Cramer"],
["Labyrinth","Jennifer Connelly"],
["Last Action Hero","Arnold Schwarzenegger"],
["Legend","Tim Curry"],
["Leon","Jean Reno"],
["Live and Let Die","Roger Moore"],
["Mad Max: Beyond Thunderdome","Mel Gibson"],
["Masters of the Universe","Dolph Lundgren"],
["Moonraker","Roger Moore"],
["Nightcrawler","Jake Gyllenhaal"],
["Nothing But Trouble","Chevy Chase"],
["On Her Majesty's Secret Service","George Lazenby"],
["Poltergeist","Craig T. Nelson"],
["Pulp Fiction","John Travolta"],
["Raging Bull","Robert De Niro"],
["Raiders of the Lost Ark","Karen Allen"],
["Return of the Living Dead Part II","James Karen"],
["Return of the Living Dead","Clu Gulager"],
["Robocop","Peter Weller"],
["Star Trek_ The Wrath of Khan","William Shatner"],
["Star Wars - Episode III: Revenge of the Sith","Ewan McGregor"],
["Star Wars - Episode II: Attack of the Clones","Natalie Portman"],
["Star Wars - Episode IV: A New Hope","Mark Hamill"],
["Star Wars - Episode I: The Phantom Menace","Liam Neeson"],
["Star Wars - Episode VI: Return of the Jedi","Carrie Fisher"],
["Star Wars - Episode V: The Empire Strikes Back","Billy Dee Williams"],
["The Bodyguard","Whitney Houston"],
["The Exoricist","Ellen Burstyn"],
["The Fog","Adrienne Barbeau"],
["The Frightners","Michael J. Fox"],
["The Godfather","Marlon Brando"],
["The Goonies","Sean Astin"],
["The Grand Budapest Hotel","Ralph Fiennes"],
["The Muppet Movie","Jim Henson"],
["The Neverending Story","Barret Oliver"],
["The Spy Who Loved Me","Barbara Bach"],
["The Ten Commandments","Charlton Heston"],
["The Thing","Kurt Russell"],
["THX 1138","Robert Duvall"],
["Tremors","Kevin Bacon"],
["Vacation","Chevy Chase"],
["Willow","Warwick Davis"]];

var intervalId = null;
var score = 0;
var best_score = 0;  // Meilleur score
var total_score = 0;  // Score total pour le suivi des stats
var total_succes = 0;  // pour les stats
var total_failure = 0;  // pour les stats
var consecutive = 0;
function finish() {
  clearInterval(intervalId);	
  // Get the modal
  var modal = document.getElementById("myModal");
  // Get the game
  var game = document.getElementById("game");
  // Score Field
  document.getElementById("finalBestScore").innerHTML = "" + best_score;
  
  modal.style.display = "block";
  game.style.display = "none";
  //window.location.href='index.php';
}
function saveScore() {
  window.location.href='game.php';
}
function bip() {
    counter--;
    if(counter == 0) finish();
    else if (counter<10){
        document.getElementById("bip").innerHTML = "00 : "+"0"+counter;
    }
    else {	
        document.getElementById("bip").innerHTML = "00 : "+counter;
    }	
}
window.onload=function start(){
  intervalId = setInterval(bip, 1000);
  reset();
  
}	
function wrong_choice(e){
  var target = e.target;
  total_failure += 1;
  if(score>50){
    score -=50;
  }
  else{
    score = 0;
  }
  total_score -=50;
  consecutive = 0;
  document.getElementById("score").innerHTML = " " + score;
  target.className += " WrongAnswer";
  setTimeout(reset,500);
}
function right_choice(e){
  var target = e.target;
  target.className += " CorrectAnswer";
  score += 100+consecutive;
  consecutive+=10;
  if (score > best_score){
    best_score = score;
  }
  total_score += 100;
  total_succes += 1;
  document.getElementById("score").innerHTML = " " + score;
  document.getElementById("best_score").innerHTML = " " + best_score;
  setTimeout(reset,500);
}
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
var already = []
function reset(){
  var nb = getRandomInt(80);
  while(already.includes(nb)){
    nb=getRandomInt(80);
  }
  already.push(nb);
  title = filmList[nb][0];
  document.getElementById("poster").setAttribute("src", "resources/films/"+title+".jpg");
  var bb = getRandomInt(4)+1;
  var button = document.getElementById("button-"+bb);
  button.onclick=function(){right_choice(event);};
  button.innerHTML = filmList[nb][1];
  alreadytmp=[nb];
  for (var i = 1; i < 5; i++) {
    if(i!=bb){
      var button = document.getElementById("button-"+i);
      button.onclick=function(){wrong_choice(event);}
      acb = getRandomInt(80);
      while(alreadytmp.includes(acb)){
        acb = getRandomInt(80);
      }
      alreadytmp.push(acb);
      button.innerHTML = filmList[acb][1];
    }
  }
  document.getElementById("title").innerHTML = title;
  document.getElementById("button-1").className=" ActorButton";
  document.getElementById("button-2").className=" ActorButton";
  document.getElementById("button-3").className=" ActorButton";
  document.getElementById("button-4").className=" ActorButton";
}


</script>

