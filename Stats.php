<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8" />
		<title>Statistiques</title>
		<link rel="stylesheet" href="main.css" type="text/css">
    </head>
    <body class="BG">
        <header class="Game-header">
            <div class="Logo"><img class="Logo" src="resources/MovIIEQuizz.png" alt="Logo"></div>
        </header>
        <div class="MainBody">
            <p class="Text"> <b> Voici vos Statistiques </b></p><br/>
            <p class="Text"> <b> Meilleur score: </b><span id="best_score"></span><p>
            <p class="Text"> <b> Total de points gagnés: </b><span id="total_score"></span></p><br/>
            <p class="Text"> <b> Total de succès: </b><span id="total_succes"></span></p><br/>
            <p class="Text"> <b> Total d'echecs: </b><span id="total_failure"></span> </p><br/>
            <p class="Text"> <b> Pourcentage de succès: </b><span id="percent_succes"></span> %</p><br/>
        </div>
    </body>

    <script type="text/javascript"> 

        function percent_succes(succes, echecs){
            percent = (succes / (succes + echecs)) * 100
            return percent.toFixed(2);
        }
        var total_score = 1500;
        var total_succes = 15;
        var total_failure = 2;
        var best_score = 4000;
        document.getElementById("total_score").innerHTML=total_score;
        document.getElementById("total_succes").innerHTML=total_succes;
        document.getElementById("total_failure").innerHTML=total_failure;
        document.getElementById("percent_succes").innerHTML=percent_succes(total_succes, total_failure);
        document.getElementById("best_score").innerHTML=best_score;
    </script>
</html>


