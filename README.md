# Groupe n°19

### Composition de l'équipe

| Nom          | Prénom        | Email                         |
| -------------|---------------|-------------------------------|
| zamouri      | jawhar        | jawhar.zamouri@ensiie.fr      |
| BIYADI       | Mohamed Ayman | mohamedayman.biyadi@ensiie.fr |
| DEROUET      | TRISTAN       | tristan.derouet@ensiie.fr     |
| SAMLALI      | Youssef       | youssef.samlali@ensiie.fr     |
| KANOUNI      | Youssef       | youssef.kanouni@ensiie.fr     |
| ICHALALEN    | Lyes          | lyes.ichalalen@ensiie.fr      |

### Sujet : Un jeux dont le but est de deviner un des acteur principaux de film plus ou moins connu

### Adresse du backlog : https://trello.com/b/09WrCsW0/pima