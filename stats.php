<?php
// connect to database 
    include_once 'database.php';
    $res = mysqli_query($conn,"SELECT player.name as 'player', sum(score) as 'total score', max(score) as 'best score', count(*) as 'games played' from game inner join player on player.id = game.playerId GROUP BY player.name ORDER BY score DESC limit 10");
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8" />
		<title>Statistiques</title>
		<link rel="stylesheet" href="main.css" type="text/css">
    </head>
    <body class="BG">
        <header class="Game-header">
            <div class="Logo"><img class="Logo" src="resources/MovIIEQuizz.png" alt="Logo"></div>
        </header>
        <div class="MainBody">
        <?php 
        if (mysqli_num_rows($res) > 0) { 
        ?>

        <h2 class="Title">Top 10 Best Scores</h2>

        <table class="table-fill">
            <tr>
                <th>Player</th>
                <th>Total Score</th>
                <th>Best Score</th>
                <th>Games Played</th>
            </tr>
            <?php 
                $i=0;
                while($row = mysqli_fetch_array($res)) { 
            ?>
            <tr>
                <td><?php echo $row["player"]; ?></td>
                <td><?php echo $row["total score"]; ?></td>
                <td><?php echo $row["best score"]; ?></td>
                <td><?php echo $row["games played"]; ?></td>
            </tr>
            <?php 
                $i++;
                } 
            ?>
        </table>
        <?php
            }
            else{
                echo "No result found";
            }
        ?>
        </div>
    </body>
</html>
