create database moviiequiz;
use moviiequiz;

CREATE TABLE Player (
	id integer primary key not null auto_increment,
	name varchar(30)
);

CREATE TABLE Game (
	id integer primary key not null auto_increment,
	score integer,
	time integer,
	playerId integer,
    FOREIGN KEY (playerId) REFERENCES Player(id)
);

INSERT INTO Player (name) VALUES ("zer0");
INSERT INTO Player (name) VALUES ("retr0");
INSERT INTO Player (name) VALUES ("ruby");


INSERT INTO Game (score, time, playerId) VALUES (600, 90, 2);
INSERT INTO Game (score, time, playerId) VALUES (800, 50, 2);
INSERT INTO Game (score, time, playerId) VALUES (300, 30, 2);

INSERT INTO Game (score, time, playerId) VALUES (700, 60, 1);

select sum(score) from game inner join player on player.id = game.playerId where player.name = "retr0";
select max(score) from game inner join player on player.id = game.playerId where player.name = "retr0";