# Rapport d'itération  


## Composition de l'équipe 


|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | BIYADI Mohamed Ayman     |
| **Scrum Master**        | Youcef Samlali           |

## Bilan de l'itération précédente  
### Évènements 
> On a organisé des tests dans le foyer pour avoir des retours utilisateurs sur notre jeu\
> On s'est concerté lors d'une réunion pour discuter des retours et des actions à prendre en conséquence\
> Une deuxième série de test a été organisé avec d'autres personnes pour voir si les changements étaient positifs.




### Taux de complétion de l'itération  
> L'itération ayant été repensée, on peut considérer avec les changements qui ont eu lieu que la quasi-totalité des fonctionnalités ont été implémentées : 90%\


### Liste des User Stories terminées
> En tant qu'utilisateur je veux pouvoir voir mon score à la fin de la partie pour voir mon niveau.\
> En tant qu'utilisateur je veux pouvoir enregistrer mon score simplement pour jouer de manière plus fluide.\
> En tant qu'utilisateur je veux être récompensé de manière juste et cohérente pour que mon score reflète mes connaissances réelles.\
> En tant qu'utilisateur je veux pouvoir voir les meilleurs scores qui ont pu être fait sur le jeu pour pouvoir me mesurer aux autres\
> En tant qu'utilisateur je veux pouvoir rejouer rapidement à la fin de la partie pour tenter d'améliorer mon score.\



## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> Les premiers tests avec les utilisateurs nous ont appris beaucoup sur les qualités et les défauts de notre jeu.\
> L'un des aspects les plus importants fut le système de login bien trop lourd pour le type de jeu que l'on était en train de faire et que l'on a donc choisi de changer complètement.\
> Les seconds tests furent vraiment concluant avec des retours majoritairement positifs.\
> Le changement de direction au niveau du projet avec le passage à un jeu plus arcade fut donc une bonne idée.\



