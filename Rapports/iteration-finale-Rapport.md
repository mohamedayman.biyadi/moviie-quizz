# Rapport d'itération  


## Composition de l'équipe 


|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | BIYADI Mohamed Ayman     |
| **Scrum Master**        | Youcef Samlali           |

### Bilan intégral des différentes itérations 
> Au début on a eu des problèmes de communication qui ont freiné l'avancée du projet.
> La mise en place de réunions régulières a permis de palier ce problème.
> Les US associés au itérations étaient parfois parfois trop ambitieuses. Nous avons donc essayé de réfléchir au maximum à la difficulté et au temps nécessaire pour chaque US avant leur intégration au sprint.
> L'appropriation des différentes US n'était pas bien organisé au début, cela c'est arrangé au fil des itérations.
> On s'est trop concentré à vouloir faire du Back end au début du projet. Afin d'avoir des présentations agréables et plus adapté à la méthode agile, on a décidé de plus se concentrer sur le Front.
> Au début on décidait seulement entre nous concernant les fonctionnalités à implémenter dans le jeu. Les tests utilisateurs organisés à la fin ont permis de mieux comprendre ce qui était plaisant dans le jeu et de mieux penser le projet.





### Taux de complétion final du projet 
> Le jeu est jouable et apprécié par les utilisateurs. Il reste du peaufinage à faire mais globalement le projet peut être considéré comme presque aboutit : 90%\



