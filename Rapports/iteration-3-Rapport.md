# Rapport d'itération  


## Composition de l'équipe 


|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | BIYADI Mohamed Ayman     |
| **Scrum Master**        | DEROUET Tristan          |

## Bilan de l'itération précédente  
### Évènements 
> Une réunion a été organisé sur les bases de données utilisées.\
> Une deuxième réunion pour discuter de la refonte graphique de l'interface.\
> Tristan a été malade durant plusieurs jours ce qui l'a empeché de travailler.\
> Des examens sont arrivés ce qui a impacté le sprint.\




### Taux de complétion de l'itération  
> Les éléments ont en majeur partie été fait mais pas de manière efficace : 60%


### Liste des User Stories terminées
> En tant qu'utilisateur, je veux pouvoir avoir une nouvelle question quand je réponds afin de continuer la partie.
> En tant qu'utilisateur, je veux un bel aspect visuel de l'application pour me donner envie de jouer.\
> En tant qu'utilisateur, je veux pouvoir avoir accès à mes statistiques pour connaître ma progression.\


## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> Il y a eu encore un manque d'investissement de la part de certains membres de l'équipe.\
> La communication s'est néanmoins bien passé.\
> On a commençé à maitriser la complexité des tâches sur le tableau.\



### Actions prises pour la prochaine itération
> Améliorer les phases de tests
> Faire essayer le jeu à d'autres personnes histoire d'avoir des avis extérieur pour l'améliorer.

 
### Axes d'améliorations 
> Le Scrum Master devrait être plus dynamique durant la review\


## Prévisions de l'itération suivante  
### Évènements prévus  
> Des examens vont avoir lieu et risquent de ralentir la progression.\
> De nouveaux projets sont arrivés ce qui vas encore ralentir la progression.\
> La grèves risque d'impacter certains membres du groupe notamment pour les réunions.



### Titre des User Stories reportées  
> En tant qu'utilisateur, je veux pouvoir privatiser mon pseudo afin de garder une trace de mes exploits.\
> En tant qu'utilisateur, je veux pouvoir choisir la difficulté du jeu afin de l'adapter à mon niveau.\
> En tant qu'utilisateur, je veux pouvoir avoir accès à mes statistiques pour connaître ma progression.\
> En tant qu'utilisateur, je veux pouvoir voir mon meilleur score pendant la partie afin de me motiver à le dépasser.


### Titre des nouvelles User Stories  
> En tant qu'utilisateur, je veux pouvoir inviter des amis afin de voir leurs scores.

## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *1* 	    |  *3* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *0* 	    |  *3* 	|  *1* 	|

 
 