# Rapport d'itération  


## Composition de l'équipe 


|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | BIYADI Mohamed Ayman     |
| **Scrum Master**        | GRANDVAUX Maxime         |

## Bilan de l'itération précédente  
### Évènements 
> Une réunion a été décidé d'une ébauche de design.\
> Une deuxième réunion a eu lieu afin de décider de la manière de procéder pour le développement.\
> Il y a eu un problème de communication sur le travail effectué.




### Taux de complétion de l'itération  
> Les éléments ont en majeur partie été fait mais pas de manière efficace : 60%


### Liste des User Stories terminées
> En tant qu'utilisateur, je souhaite avoir un moyen de lancer le jeu quand je veux.\
> En tant qu'utilisateur je veux voir des propositions d'acteurs lorsque je joue pour pouvoir sélectionner une réponse.\
> En tant qu'utilisateur, je veux avoir un score pendant que je joue afin de mesurer ma performance.\
> En tant qu'utilisateur je veux voir la photo d'un film pour pouvoir deviner quel acteur a joué dedans.\
> En tant qu'utilisateur, je veux pouvoir voir un indicateur lorsque je choisis une réponse pour savoir si elle est correcte ou non.\
> En tant qu'utilisateur je veux voir le temps qu'il me reste pour jouer afin de savoir quand la partie se finit.
## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> Les réunions n'ont pas suffit pour s'assurer de la bonne transmission des informations.\
> Il y a eu un manque d'investissement de la part de certains membres de l'équipe.\
> Les outils utilisés n'ont pas été clairs pour tout le monde.



### Actions prises pour la prochaine itération
> Améliorer la communication.\
> Faire un sprint plus adapté aux compétences de chacun.\
> Décider à l'avance de la répartition des tâches.

 
### Axes d'améliorations 
> Le Scrum Master devrait prendre plus d'initiative.\
> Le Product Owner pourrait être plus réactif au niveau des changements concernant le projet.


## Prévisions de l'itération suivante  
### Évènements prévus  
> Des examens vont avoir lieu et risquent de ralentir la progression.\
> Youssef Kanouni va devoir se faire opérer de nouveau et aura peut-être des soucis pour travailler.\
> En tant qu'utilisateur, je veux pouvoir avoir une nouvelle question quand je réponds afin de continuer la partie.



### Titre des User Stories reportées  
> En tant qu'utilisateur, je veux pouvoir avoir une nouvelle question quand je réponds afin de continuer la partie.


### Titre des nouvelles User Stories  
> En tant qu'utilisateur, je veux pouvoir passer une question lorsque je ne connais pas la réponse afin de ne pas rester bloqué.\
> En tant qu'utilisateur, je veux un bel aspect visuel de l'application pour me donner envie de jouer.\
> En tant qu'utilisateur, je veux pouvoir privatiser mon pseudo afin de garder une trace de mes exploits.\
> En tant qu'utilisateur, je veux pouvoir choisir la difficulté du jeu afin de l'adapter à mon niveau.\
> En tant qu'utilisateur, je veux pouvoir avoir accès à mes statistiques pour connaître ma progression.\
> En tant qu'utilisateur, je veux pouvoir voir mon meilleur score pendant la partie afin de me motiver à le dépasser.

## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *2* 	    |  *5* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *2* 	    |  *4* 	|  *1* 	|

 
 