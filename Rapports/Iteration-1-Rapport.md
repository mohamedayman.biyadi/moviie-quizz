# Rapport d'itération  


## Composition de l'équipe 


|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | BIYADI Mohamed Ayman     |
| **Scrum Master**        | DEROUET TRISTAN          |

## Bilan de l'itération précédente  
### Évènements 
> On a remarqué une trop haute complexité dans les User Stories
> On a remarqué que l'outil choisi pour faire le site n'était pas adapté au groupe et au projet.
> Tristan n'a pas pu être présent lors de la réunion organisée.



### Taux de complétion de l'itération  
> Aucun élément n'a pu être terminé: 0%


### Liste des User Stories terminées
> Aucune User Stories n'a pu être terminée.

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> Chacun des membres aurait pu réagir plus rapidement concernant les problèmes avec la technologie utilisée.
> Lyes Ichalalen et Jawhar Zamouri ont des difficultés avec le css.
> Tristan semble en particulier maîtriser le php.
> Les disponibilités des membres du groupe ne sont pas vraiment compatibles.


### Actions prises pour la prochaine itération
> Faire des points plus régulièrement.
> Refaire le projet avec des technologies plus simples et maitrisées par le groupe.
> Mieux organiser la répartition des tâches.

 
### Axes d'améliorations 
> Pour le Scrum Master, il pourrait y avoir une amélioration au niveau de la communication.
> Le Product Owner n'a pas su donner exactement les axes du projet.


## Prévisions de l'itération suivante  
### Évènements prévus  
> L'absence de certains pendant les vacances risque d'impacter l'itération.
> Youssef Kanouni était absent durant la séance du Jeudi 24 Octobre.
> Suite à l'arrivée d'autre projets, les membres du groupe risquent d'avoir moins de temps pour ce projet.



### Titre des User Stories reportées  
> En tant qu'utilisateur, je souhaite avoir un moyen de lancer le jeu quand je veux.
> En tant qu'utilisateur, je veux voir des propositions d'acteurs lorsque je joue pour pouvoir sélectionner une réponse.
> En tant qu'utilisateur, je veux voir le temps qu'il me reste pour jouer afin de savoir le temps que finir la partie.


### Titre des nouvelles User Stories  
> En tant qu'utilisateur, je veux voir la photo d'un film pour pouvoir deviner quel acteur a joué dedans.
> En tant qu'utilisateur, je veux pouvoir voir un indicateur lorsque je choisis une réponse pour savoir si elle est correcte ou non.
> En tant qu'utilisateur, je veux pouvoir avoir une nouvelle question quand je réponds afin de continuer la partie.
> En tant qu'utilisateur, je veux avoir un score pendant que je joue afin de mesurer ma performance.

## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *0* 	    |  *5* 	|  *1* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 19 |  *0* 	|  *1* 	    |  *3* 	|  *2* 	|

 
 